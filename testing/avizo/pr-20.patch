Patch-Source: https://github.com/misterdanb/avizo/pull/20

From fd7950951f84cd7eadeae7889c6d3f53e7e98968 Mon Sep 17 00:00:00 2001
From: Jakub Jirutka <jakub@jirutka.cz>
Date: Wed, 5 Jan 2022 15:35:37 +0100
Subject: [PATCH] Rewrite shell scripts to portable POSIX shell language

/bin/sh may not be bash and not every system even provides Bash
by default.
---
 lightctl  | 26 +++++++++++---------------
 volumectl | 55 ++++++++++++++++++++++++++++++++-----------------------
 2 files changed, 43 insertions(+), 38 deletions(-)

diff --git a/lightctl b/lightctl
index 8a49d48..5cb3f43 100755
--- a/lightctl
+++ b/lightctl
@@ -1,19 +1,16 @@
-#!/usr/bin/env bash
+#!/bin/sh
 
-me="$(basename "$0")";
-running=$(ps h -C "$me" | grep -wv $$ | wc -l);
-[[ $running > 1 ]] && exit;
+me="$(basename "$0")"
+running=$(ps h -C "$me" | grep -wv $$ | wc -l)
+[ "$running" -gt 1 ] && exit
 
 time=4
 bias=0
 
-if [[ "$1" == "lower" ]]
-then
-    light -U 5
-elif [[ "$1" == "raise" ]]
-then
-    light -A 5
-fi
+case "$1" in
+	lower) light -U 5;;
+	raise) light -A 5;;
+esac
 
 light=$(light)
 light=$(echo $light | awk '{print int($1+0.5)}')
@@ -21,14 +18,13 @@ scaled=$(echo "scale=2; $light / 100.0" | bc)
 
 alpha="0.5"
 
-if [ "$light" -lt "34" ]
+if [ "$light" -lt 34 ]
 then
     avizo-client --image-resource="brightness_low" --progress=$scaled --time=$time --background="rgba(255, 255, 255, $alpha)"
-elif [ "$light" -lt "66" ]
+elif [ "$light" -lt 66 ]
 then
     avizo-client --image-resource="brightness_medium" --progress=$scaled --time=$time --background="rgba(255, 255, 255, $alpha)"
-elif [ "$light" -lt "101" ]
+elif [ "$light" -lt 101 ]
 then
     avizo-client --image-resource="brightness_high" --progress=$scaled --time=$time --background="rgba(255, 255, 255, $alpha)"
 fi
-
diff --git a/volumectl b/volumectl
index 9346ad0..e76ac50 100755
--- a/volumectl
+++ b/volumectl
@@ -1,15 +1,22 @@
 #!/bin/sh
 
-if [ "z$VOLUME_DEBUG" != "z" ]; then
+if [ -n "$VOLUME_DEBUG" ]; then
     set -x
 fi
 
-function _current() {
-  CURRENT=$(pamixer --get-volume)
+_current() {
+  CURRENT=$(pactl list sinks |\
+    grep '^[[:space:]]Volume:' |\
+    head -n $(( $DEF_SINK_N + 1 )) |\
+    tail -n 1 |\
+    sed -e 's,.* \([0-9][0-9]*\)%.*,\1,')
 }
-function _is_muted() {
-
-  OUT_MUTED=$(if $(pamixer --get-mute) == false; then echo yes; else echo no; fi)
+_is_muted() {
+  OUT_MUTED=$(pactl list sinks |\
+    grep '^[[:space:]]Mute:' |\
+    head -n $(( $DEF_SINK_N + 1 )) |\
+    tail -n 1 |\
+    awk -F": " '{print $2}')
 
   IN_MUTED=$(pactl list sources|\
     grep '^[[:space:]]\(Mute\|Name\)'|\
@@ -20,51 +27,53 @@ function _is_muted() {
 
 AMOUNT="5%"
 TIME=1
-DEF_SOURCE=$(pactl info | grep "Default Source: "| awk -F": " '{print $2}')
+DEF_SINK=$(pactl info | grep "Default Sink: " | awk -F": " '{print $2}')
+DEF_SINK_N=$(pactl list short sinks | grep "$DEF_SINK" | cut -f1)
+DEF_SOURCE=$(pactl info | grep "Default Source: " | awk -F": " '{print $2}')
 
-function _maybe_unmute() {
+_maybe_unmute() {
   _is_muted
-  if [[ "$OUT_MUTED" = "yes" ]]; then
+  if [ "$OUT_MUTED" = "yes" ]; then
     pactl set-sink-mute @DEFAULT_SINK@ false
   fi
 }
 
-function _raise() {
+_raise() {
   _maybe_unmute
   _current
-  if [ ${CURRENT} -lt 100 ]; then
+  if [ "$CURRENT" -lt 100 ]; then
       pactl set-sink-volume @DEFAULT_SINK@ "+${AMOUNT}"
   fi
 }
 
-function _lower() {
+_lower() {
   _maybe_unmute
   pactl set-sink-volume @DEFAULT_SINK@ "-${AMOUNT}"
 }
 
-function _toggle_out_mute() {
+_toggle_out_mute() {
   pactl set-sink-mute @DEFAULT_SINK@ toggle
 }
 
-function _toggle_in_mute() {
+_toggle_in_mute() {
   for SOURCE in $(pactl list short sources | grep -v monitor | cut -f1)
   do
     pactl set-source-mute $SOURCE toggle
   done
 }
 
-function _notify() {
+_notify() {
   local alpha="0.5"
 
   _is_muted
   _current
-  if [[ "$1" == "out-mute" && "$OUT_MUTED" == "yes" ]]
+  if [ "$1" = "out-mute" ] && [ "$OUT_MUTED" = "yes" ]
   then
       avizo-client --image-resource="volume_muted" --progress="0" --time=$TIME --background="rgba(255, 255, 255, $alpha)"
-  elif [[ "$1" == "in-mute" && "$IN_MUTED" == "yes" ]]
+  elif [ "$1" = "in-mute" ] && [ "$IN_MUTED" = "yes" ]
   then
       avizo-client --image-resource="mic_muted" --time=$TIME --background="rgba(255, 255, 255, $alpha)"
-  elif [[ "$1" == "in-mute" && "$IN_MUTED" == "no" ]]
+  elif [ "$1" = "in-mute" ] && [ "$IN_MUTED" = "no" ]
   then
       avizo-client --image-resource="mic_unmuted" --time=$TIME --background="rgba(255, 255, 255, $alpha)"
   else
@@ -83,27 +92,27 @@ function _notify() {
   fi
 }
 
-function raise() {
+raise() {
   _raise
   _notify
 }
 
-function lower() {
+lower() {
   _lower
   _notify
 }
 
-function toggle_out_mute() {
+toggle_out_mute() {
   _toggle_out_mute
   _notify out-mute
 }
 
-function toggle_in_mute() {
+toggle_in_mute() {
   _toggle_in_mute
   _notify in-mute
 }
 
-function help() {
+help() {
   cat <<EOH
 $0 [COMMAND]
 Control the volume of the default sink and displays the current outcome.
