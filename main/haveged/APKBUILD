# Contributor: Bartłomiej Piotrowski <nospam@bpiotrowski.pl>
# Contributor: Danilo Godec <danilo.godec@agenda.si>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=haveged
pkgver=1.9.16
pkgrel=0
pkgdesc="Entropy harvesting daemon using CPU timings"
url="https://www.issihosts.com/haveged/"
arch="all"
license="GPL-3.0-or-later"
subpackages="$pkgname-dev $pkgname-doc $pkgname-openrc"
makedepends="autoconf automake libtool linux-headers"
source="$pkgname-$pkgver.tar.gz::https://github.com/jirka-h/haveged/archive/v$pkgver.tar.gz
	haveged.initd
	haveged.confd"

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	rm "$pkgdir"/usr/lib/libhavege.la

	install -Dm755 "$srcdir"/haveged.initd \
		"$pkgdir"/etc/init.d/haveged
	install -Dm644 "$srcdir"/haveged.confd \
		"$pkgdir"/etc/conf.d/haveged
}

sha512sums="
89c1718ac8ee228938bf8ffb0ca6c77fe11dedc9cb484fb20492617a9c2f9e264059a73c688d9f4945117b147fdd15586317a8a3b7dbc257ca5b19af00aab076  haveged-1.9.16.tar.gz
99dfe4e40b99041a34cfff8539d6148c88413d2fadc2ab8db7edda95f8c84e21b61643de51855d70f0f6d20bbe1f46fc8198bfd5d512099545fad74d1c7132b6  haveged.initd
c2dcaf151d314500c9147b97ce08bb65c902431ac1c60e76072f5237164fa4ff4aa9115eba9329fffb14add7759e4d9acc894bcf49dcc4ea37339a01c2aa1ed7  haveged.confd
"
