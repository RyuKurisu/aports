# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=ruby-bundler
_gemname=bundler
pkgver=2.3.4
pkgrel=0
pkgdesc="Manage an application's gem dependencies"
url="https://bundler.io/"
arch="noarch"
license="MIT"
depends="ruby"
makedepends="ruby-rake"
subpackages="$pkgname-doc"
source="https://github.com/rubygems/rubygems/archive/bundler-v$pkgver.tar.gz
	manpages.patch
	"
builddir="$srcdir/rubygems-bundler-v$pkgver/bundler"
options="!check"  # tests require deps not available in main repo

build() {
	rake build_metadata
	gem build $_gemname.gemspec
}

package() {
	local gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"

	gem install \
		--local \
		--install-dir "$gemdir" \
		--bindir "$pkgdir/usr/bin" \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname

	local n; for n in 1 5; do
		mkdir -p "$pkgdir"/usr/share/man/man$n
		mv "$gemdir"/gems/$_gemname-$pkgver/lib/bundler/man/*.$n "$pkgdir"/usr/share/man/man$n/
	done

	rm -rf "$gemdir"/cache \
		"$gemdir"/build_info \
		"$gemdir"/doc \
		"$gemdir"/gems/$_gemname-$pkgver/man \
		"$gemdir"/gems/$_gemname-$pkgver/*.md
}

sha512sums="
f3ab69615ca3d95eddce94a2f164df0a27b48ccc85cc175c025d4b3429a190d3ba433da538d9552033886a7064ad60148faaa6b5a10920f7dd5eacf20bfe09c2  bundler-v2.3.4.tar.gz
5c9cc8046120360f9daa3d94da092c8db452672b8fb46b1a8188fae690e4362a090c0fcce35487399aedfec2f7f6b212c0d873a065475871b76d09313964596a  manpages.patch
"
